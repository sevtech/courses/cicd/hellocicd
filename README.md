# CI/CD Application

This is a project to test continuous integration and delivery with two of the most famous alternatives as <b>Jenkins</b> and <b>Gitlab CI</b>.

## Pre-requisites
* Install Jenkins (https://jenkins.io/doc/book/installing/#war-file).
* Google Cloud account.
* SDK Cloud (https://cloud.google.com/sdk/?hl=es).
* Gitlab account.
* Docker installed.
* Maven installed.
* Fork this repo.

## Configure Jenkins

* Add https://gitlab.com/sevtech/courses/cicd/shared-libraries.git to <a href="https://jenkins.io/doc/book/pipeline/shared-libraries/">shared libraries</a>.

## Google Cloud configuration

* Create service account:
```sh  
gcloud iam service-accounts create [SA-NAME] --display-name "[SA-DISPLAY-NAME]" 
```

* Create service account key:
```sh  
gcloud iam service-accounts keys create key.json --iam-account [SA-NAME]@[PROJECT-ID].iam.gserviceaccount.com
```

* Activate Container Registry API in Google Cloud Console.

* Activate Cloud Run API in Google Cloud Console.

## Gitlab configuration
* Replace <b>sevtech-265118</b> from .gitlab-ci.yml by your project id.
* Add your service account key to CI/CD variables settings with type <b>file</b> and key <b>GOOGLE_SERVICE_ACCOUNT_FILE</b>.

## Authors

Miguel Ángel Quintanilla y Francisco Javier Delgado Vallano.